const todosData = [
    {
        id: 1,
        text: 'Learn basic javascript',
        completed: true
    },
    {
        id: 2,
        text: 'Read some mode about javascript',
        completed: false
    },
    {
        id: 3,
        text: 'Learn about HTML',
        completed: true
    },
    {
        id: 4,
        text: 'Practice using HTML',
        completed: false
    },
    {
        id: 5,
        text: 'Learn basic CSS',
        completed: true
    }
]

export default todosData;